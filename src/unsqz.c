/*
 * Copyright (C) 2008 Eirik Stople
 *
 * OpenTitus is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General  Public License as published
 * by the Free  Software Foundation; either version 3  of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
 * MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
 * General Public License for more details.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "sqz.h"


int main (int argc, char *argv[]) {
  unsigned char *test = NULL;
  int retval;
  FILE *fi;
  unsigned char sign[4];
  char *outname = argv[2];
  //
  if (argc < 2) {
    fprintf(stderr, "usage: unsqz infile [outfile]\n");
    return 1;
  }
  //
  fi = fopen(argv[1], "rb");
  if (fi == NULL) {
    fprintf(stderr, "FATAL: can't open input file: '%s'\n", argv[1]);
    return 1;
  }
  //
  if (fread(sign, 4, 1, fi) != 1) {
    fclose(fi);
    fprintf(stderr, "FATAL: can't read input file: '%s'\n", argv[1]);
    return 1;
  }
  fclose(fi);
  //
  if (sign[0] == 0xb4 && sign[1] == 0x4c && sign[2] == 0xcd && sign[3] == 0x21) {
    fprintf(stderr, "FATAL: DIET compression found in file: '%s'\n", argv[1]);
    return 1;
  }
  //
  if (sign[1] != 0 && sign[1] != 0x10) {
    fprintf(stderr, "FATAL: unknown compression method in file: '%s'\n", argv[1]);
    return 1;
  }
  //
  if (argc == 2) {
    outname = calloc(strlen(argv[1])+128, 1);
    if (outname == NULL) abort();
    sprintf(outname, "%s.unp", argv[1]);
  }
  //
  printf("unpacking %s '%s' to '%s'...\n", sign[1]==0?"LZW":"HUFF+RLE", argv[1], outname);
  retval = unSQZ(argv[1], &test);
  //printf("retval = %x\n", retval);
  //if (test == NULL) printf("test = NULL\n");
  if (retval > 0 && test != NULL) {
    FILE *ofp = fopen(outname, "wb");
    //
    if (ofp == NULL) {
      free(test);
      fprintf(stderr, "FATAL: can't create output file: '%s'\n", outname);
      return 1;
    }
    if (fwrite(test, retval, 1, ofp) != 1) {
      fclose(ofp);
      unlink(outname);
      free(test);
      fprintf(stderr, "FATAL: can't write output file: '%s'\n", outname);
      return 1;
    }
    fclose(ofp);
    free(test);
    return 0;
  }
  if (test != NULL) free(test);
  //
  return 1;
}
